#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include "test1.h"
#include "malloc.h"


static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void run_test_1() {
    printf("%s", "Обычное успешное выделение памяти.\n");
    void *a = _malloc( 1000 );
    printf("%s", "jopa\n");
    debug_heap(stdout, HEAP_START);
    printf("%zu", block_get_header(a)->capacity.bytes);
}
