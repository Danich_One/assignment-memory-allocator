#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    printf("%s", "alloc_region start\n");
  struct region reg;
  reg.addr = map_pages(addr, query, MAP_FIXED_NOREPLACE);
  reg.size = region_actual_size(query);
  reg.extends = false;
  if (reg.addr == MAP_FAILED) {
    reg.addr = map_pages(addr, query, 0);
    if (reg.addr == MAP_FAILED) {
        return REGION_INVALID;
    }
  }
    else {
        reg.extends = true;
    }
  block_init(reg.addr, (block_size){ .bytes = query }, NULL);
    printf("%s", "alloc_region finish\n");
  return reg;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    printf("%s", "split_if_too_big start\n");
  if (block_splittable(block, query)) {
    block_capacity cap = { .bytes = query };
    block_capacity next_cap = { .bytes = block->capacity.bytes-size_from_capacity(cap).bytes };
    struct block_header* next_block = block + size_from_capacity(cap).bytes;
    next_block->capacity = next_cap;
    next_block->is_free = true;
    block->next = next_block;
    block->capacity = cap;
    block->is_free = true;
    return true;
  }
    printf("%s", "split_if_too_big finish\n");
  return 0;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  printf("%s", "try_merge_with_next start\n");
  //printf("%p", block);
  if (block->next != NULL){
    printf("%s", "jopa\n");
    if (mergeable(block, block->next)) {
      struct block_header* snd_block = block->next;
      block->next = snd_block->next;
      block->capacity.bytes += size_from_capacity(snd_block->capacity).bytes;
      block->is_free = true;
      return true;
    }
  }
  printf("%s", "try_merge_with_next finish\n");
  return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  printf("%s", "find_good_or_last start\n");
  struct block_search_result res = {0};
  struct block_header *temp = block;
  
  while (temp != NULL) {
    while (try_merge_with_next(temp)) { }
    if (block_is_big_enough(sz, temp)) {
      printf("%s", "jopa\n");
      res.type = BSR_FOUND_GOOD_BLOCK;
      res.block = block;
      return res;
    }
    
    res.type = BSR_REACHED_END_NOT_FOUND;
    res.block = temp;
    temp = temp->next;
    
  }
    printf("%s", "find_good_or_last finish\n");
  return res;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    printf("%s", "try_memalloc_existing start\n");
  struct block_search_result res;
  size_t max = 0;
  if (query >= BLOCK_MIN_CAPACITY) { max = query; }
  else { max = BLOCK_MIN_CAPACITY; }
  res = find_good_or_last(block, max);
  if (res.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(block, max);
  }
    printf("%s", "try_memalloc_existing finish\n");
  return res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  struct block_header* next_block = last + size_from_capacity(last->capacity).bytes;
  last->next = next_block;
  alloc_region(next_block, query);
  split_if_too_big(next_block, query);
  return next_block;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  printf("%s", "memalloc start\n");
  struct block_search_result bsr;
  bsr = try_memalloc_existing(query, heap_start);
  if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
    printf("%s", "memalloc finish\n");
    return bsr.block;
  }
  printf("%s", "memalloc finish\n");
  return grow_heap(bsr.block, query); 
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (header->next->is_free == true) {
    try_merge_with_next(header);
  }
}
